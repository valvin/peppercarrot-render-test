#!/bin/sh

export EPISODE=$(cat $WEBCOMICS_DIR/episode.txt)
export LANG=$(cat $WEBCOMICS_DIR/lang.txt)
export LAST_MR=$(cat $WEBCOMICS_DIR/last_mr.txt)
export THEME_DIR="/home/carrot/render-theme"
export RENDER_URL="https://valvin.frama.io/peppercarrot-render-test"
export GITLAB_API_URL="https://framagit.org/api/v4/projects/19067"

# grab MR info if exists
if [[ "${LAST_MR}" != "master" ]]; then
  IS_MR=1
  LAST_MR_IID=`echo $LAST_MR|jq .iid| sed s/\"//g`
  LAST_MR_USERNAME=`echo $LAST_MR|jq .author.username| sed s/\"//g`
  LAST_MR_TITLE=`echo $LAST_MR|jq .title| sed s/\"//g`
  LAST_MR_URL=`echo $LAST_MR|jq .web_url| sed s/\"//g`
else
  IS_MR=0
fi

# update them from scratch each time
if [[ -e $THEME_DIR ]]; then rm -rf $THEME_DIR; fi
git clone https://framagit.org/valvin/peppercarrot-render-theme.git /home/carrot/render-theme

# generate static galery website
if [[ $IS_MR -eq 1 ]]; then
  GALLERY_TITLE="#$LAST_MR_IID : Render $EPISODE (in $LANG) from $LAST_MR_USERNAME: $LAST_MR_TITLE "
else
  GALLERY_TITLE="Rendering $EPISODE (in $LANG) from master"
fi

mkdir public
thumbsup --input $WEBCOMICS_DIR/$EPISODE/render/$LANG --output public/ --title "$GALLERY_TITLE" --theme-path /home/carrot/render-theme

# notify on mattermost and add a comment to MR if exists
#if [[ $IS_MR -eq 1 ]]; then
#  NOTIFY_MESSAGE="New render from $LAST_MR_USERNAME for episode $EPISODE in $LANG : $RENDER_URL"
#  MR_MESSAGE="@$LAST_MR_USERNAME a preview of your merge request is available [here]($RENDER_URL)"
#  curl --request POST --header "PRIVATE-TOKEN: $GITLAB_API_KEY" -d "body=$MR_MESSAGE" $GITLAB_API_URL/merge_requests/$LAST_MR_IID/notes
#
#else
#  NOTIFY_MESSAGE="New render from master for episode $EPISODE in $LANG : $RENDER_URL"
#fi
#
#curl -i -X POST -H 'Content-Type: application/json' -d "{\"text\": \"$NOTIFY_MESSAGE\"}" https://framateam.org/hooks/$MATTERMOST_HOOK_KEY
