This project aims to ease the rendering of [Pepper&Carrot](https://www.peppercarrot.com) episode for translators.

CI is triggered when a new merge request is done in P&C [Webcomics repo](https://framagit.org/peppercarrot/webcomics)

It identifies the merge request then tries to identify corresponding episode and lang.

It generates that episode and then publish it using [thumbsup](https://thumbsup.github.io/) with a specific [theme](https://framagit.org/valvin/peppercarrot-render-theme)

It adds a comment on the merge request and notifies on Mattermost bridge to IRC using [Matterbridge](https://github.com/42wim/matterbridge).

The docker image used Gitlab CI is on Gitlab.com : [Repo](https://gitlab.com/valvin/hereva-rendrerer)

Episodes are generated using the render farm lite in P&C [tools repo](https://framagit.org/peppercarrot/tools)
