#!/bin/sh
GITLAB_API_URL="https://framagit.org/api/v4/projects/19067"

# check if webcomics repo is in cache and delete latest rendering
if [[ -e $WEBCOMICS_DIR ]]; then 
  export CACHED=1 
  rm -rf $WEBCOMICS_DIR/*/render
else 
  git clone $WEBCOMICS_REPO $WEBCOMICS_DIR
fi

cd $WEBCOMICS_DIR

# if cache exists go to master and update
if [[ ! -z $CACHED ]]; then 
  git checkout master 
  git pull
fi

# get the last opened MR and checkout the corresponding branch
LAST_MR=`curl "$GITLAB_API_URL/merge_requests?state=opened&sort=asc"|jq .[0]`
LAST_MR_BRANCH=`echo $LAST_MR|jq .source_branch | sed s/\"//g`

echo "identified merge request branch : $LAST_MR_BRANCH"
if [[ ! -z LAST_MR_BRANCH && "${LAST_MR_BRANCH}" != "null" ]]; then 
  IS_MR=1 
  git checkout $LAST_MR_BRANCH 
else
  echo "stay on master no opened merge request"
fi

# identify episode number and lang from last commits
export EPISODE=$(git log -n3 --pretty="" --name-only|grep "ep"|head -n1|awk -F '/' '{print $1 }')
export LANG=$(git log -n3 --pretty="" --name-only|grep "ep"|head -n1|awk -F '/' '{print $3 }')

# render episode using renderfarm lite
cd $WEBCOMICS_DIR/$EPISODE
echo "Rendering $EPISODE in $LANG"
/home/carrot/renderfarm-lite.py $LANG

# convert PNG to JPEG in order to minimize size
echo "converting png to jpeg"
mogrify -format jpeg $WEBCOMICS_DIR/*/render/*/*.png 

# save variables for the next stage
echo $EPISODE > $WEBCOMICS_DIR/episode.txt
echo $LANG > $WEBCOMICS_DIR/lang.txt
echo "IS_MR = $IS_MR"
if [[ $IS_MR -eq 1 ]]; then
  echo $LAST_MR > $WEBCOMICS_DIR/last_mr.txt
else
  echo "master" > $WEBCOMICS_DIR/last_mr.txt
fi
